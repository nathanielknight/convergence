#!/usr/bin/python

import re, os
import numpy as np
import matplotlib.pyplot as plt
from helpers import ij_tools
from helpers.datadir import DataDir


#Metadata
plots_dir = "./plt/"

#Load Data
#--cost function
cfns = os.popen("cat ./raw_data/cfn/cfn*").readlines()
cfns = [map(float, fields) for fields in [l.split() for l in cfns]]
cfns = {k:v for k,v in cfns}
#--gradients
dd = ij_tools.IJDir("./raw_data/gdt")
#--verify gradients iterations and cost-function interations are the
#same
cfn_iters = set(cfns.keys())
gdt_iters = set([float(fn.split('.')[-2]) for fn in dd.filenames])
assert cfn_iters == gdt_iters, \
    "Warning: Cost Func iterations != gradient iterations"


#Gradient Norm Calculations
def norm(ij):
    return sum(np.abs(ij[:,-1]))

def iter_norm(ijs):
    return sum([norm(ij) for ij in ijs])

def get_iters(dd):
    for i in map(int, cfns.keys()):
        print "gradients for iteration {}".format(i)
        gradfiles = dd.filter(lambda fn: str(i) in fn)
        for fn in gradfiles.keys():
            print "   ", fn
        yield i, dd.filter(lambda fn: str(i) in fn).values()

grad_norms = {i:iter_norm(ijs) for i,ijs in get_iters(dd)}


#Plotting Functions
def plot(iters, cfns, gdts):
    plt.xlabel("Iteration")
    plt.ylabel("Relative Cost Function")
    plt.title("Cost Function Evolution")
    
    rcfs = [cf/cfns[0] for cf in cfns]
    rgds = [g/gdts[0] for g in gdts]

    ymax = 1.2 * max(rcfs + rgds)
    plt.ylim([0, ymax])
    
    plt.plot(iters, rcfs,
             'ko', label="Cost Func.")
    #plt.plot(iters, rgds,
    #         'kx', label="|Gradient|")
    plt.legend(loc="lower left")

    plt.savefig("plt/convergence.png")



if __name__ == "__main__":
    iters = map(int, cfns.keys())
    cfns = [cfns[i] for i in iters]
    gdts = [grad_norms[i] for i in iters]
    plot(iters, cfns, gdts)
    
