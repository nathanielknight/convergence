#!/bin/bash


RUNDIR="/data2/natep/acshual_runs/gcadj_2000/runs/v8-02-01/geos4"

#get gradients
rsync -avz --delete -e "ssh -p 23"\
    "natep@stetson.phys.dal.ca:${RUNDIR}/results/ems*"\
    "./raw_data/gdt"



#get cfns
rsync -avz --delete -e "ssh -p 23"\
    "natep@stetson.phys.dal.ca:${RUNDIR}/OptData/cfn*" \
    "./raw_data/cfn" 
    
